<div>
{{--    loader style--}}
    <style>
        .loader {
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>

{{--    loader--}}
    <div class="loader" wire:loading wire:target="save"></div>

    <h1 wire:loading.class="white">posts page</h1>
    <form wire:submit.prevent="save" class="d-flex">
        <div>
            <input type="text" wire:model="title" placeholder="title" wire:loading.attr="disabled">
            @error('title')
                <span>{{$message}}</span>
            @enderror
            <span wire:dirty wire:target="title">title is typing ...</span>
        </div>
        <div>
            <textarea rows="5" placeholder="content"  wire:model="content"></textarea>
            @error('content')
            <span>{{$message}}</span>
            @enderror
        </div>
        <div>
            <input type="file" wire:model="image">
            @error('image')
            <span>{{$message}}</span>
            @enderror
        </div>
        <div>
            <button type="submit" wire:loading.remove>add</button>
        </div>
    </form>
    <hr>
    <ul wire:init="setLoaded">
        @foreach($posts as $post)
            <li>
                <h3>{{$post->title}}</h3>
                <p>{{$post->content}}</p>
                @if($post->image)
                <img src="{{$post->image}}" alt="{{explode('/',$post->image)[2]}}">
                @endif
            </li>
        @endforeach
    </ul>
</div>
