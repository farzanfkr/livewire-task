<div>
    <div>
        <h1>{{$title}}</h1>
    </div>
    <div>
        <h2>{{$title2}}</h2>
    </div>
    <div>
        <button wire:click="resetProperties">reset</button>
        <input type="text" wire:model="title">
    </div>

    <div>
        <button wire:click="$emit('showAlert')">show alert</button>
    </div>
    <div>
        <h2 wire:offline>you are offline</h2>
    </div>

    <div wire:poll.1s>
        {{now()}}
    </div>

{{--    <div wire:click.prefetch="someFunction">--}}
{{--        add prefetch--}}
{{--    </div>--}}

    <livewire:alert/>
</div>
