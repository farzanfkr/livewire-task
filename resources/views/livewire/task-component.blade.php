<div>
    <div>
        <div>{{$user->name}}</div>
        <div>
            <input type="text" wire:model="newTask">
            <button wire:click={{$editTaskId ? "editTask" : "addTask"}} >
               {{ $editTaskId ? 'edit' : 'add'}}
            </button>
        </div>
        <ul>
            @foreach($allTasks as $task)
                <li>
                    <div class="flex">
                        <button class="btn" wire:click="deleteTask({{$task->id}})">delete</button>
                        <button class="btn"  wire:click="goToEditTask({{$task->id}})">edit</button>
                        <div> {{$task->name}} </div>
                    </div>

                </li>
            @endforeach
        </ul>
        {{$allTasks->links()}}
    </div>
</div>
