<?php

namespace App\Http\Livewire;

use App\Models\Task;
use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class TaskComponent extends Component
{
    use WithPagination;

    public mixed $user;
    public string $newTask;
    public mixed $tasks;
    public int $editTaskId = 0;

    public function mount(User $user)
    {
        $this->user = $user;
        $this->newTask = '';
        $this->tasks = $user->tasks;
    }

    public function addTask()
    {
        Task::query()->create([
            'name'=> $this->newTask,
            'user_id' => $this->user->id
        ]);
        $this->tasks = Task::query()->where('user_id',$this->user->id)->get();
        $this->newTask = '';
    }

    public function deleteTask(Task $task){
        $task->delete();
       $this->tasks = Task::query()->where('user_id',$this->user->id)->get();
    }

    public function goToEditTask(Task $task)
    {
        $this->newTask = $task->name;
        $this->editTaskId = $task->id;
    }

    public function editTask()
    {
        $task = Task::query()->find($this->editTaskId);
        $task->update(['name'=>$this->newTask]);
        $this->tasks = Task::query()->where('user_id',$this->user->id)->get();
        $this->newTask = '';
        $this->editTaskId = 0;
    }

    public function render()
    {
        $allTasks = Task::query()->latest()->paginate(2);

        return view('livewire.task-component',compact('allTasks'));
    }
}
