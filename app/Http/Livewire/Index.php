<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Index extends Component
{
    public string $title = '';
    public string $title2 = '';

    public function mount()
    {
        $this->fill(['title' => 'سلام به همه', 'title2' => 'خوش آمدید']);
    }

    public function resetProperties()
    {
        $this->reset('title','title2');
    }

    public function render()
    {
        return view('livewire.index');
    }
}
