<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Alert extends Component
{
    public bool $isOpen = true;

    protected $listeners = [
        'showAlert',
    ];

    public function closeAlert()
    {
        $this->isOpen = false;
    }

    public function showAlert()
    {
        $this->isOpen = true;
    }



    public function render()
    {
        return view('livewire.alert');
    }
}
