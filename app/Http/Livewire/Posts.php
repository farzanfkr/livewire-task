<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Livewire\Component;
use Livewire\WithFileUploads;

class Posts extends Component
{
    use WithFileUploads;

    public string $title = '';
    public string $content = '';
    public bool $isLoaded = false;
    public $image;
//    public mixed $posts;

    protected $rules = [
      'content' => 'required|max:100'
    ];

    public function updatedTitle()
    {
        $this->validate([
            'title' => 'required|min:3',
        ]);
    }

    public function updatedImage()
    {
        $this->validate([
            'image' => 'image|max:512'
        ]);
    }

    public function save()
    {
        $this->validate();
        $imagePath = $this->image->store('public/images');
        Post::query()->create([
           'title' => $this->title,
           'content' => $this->content,
           'image' => 'storage/images/'.explode('/',$imagePath)[2]
        ]);
        $this->posts = Post::query()->latest()->get();
        $this->reset('title','content');
    }

    public function setLoaded()
    {
        $this->isLoaded = true;
    }

    public function mount()
    {
//        $this->posts = Post::query()->latest()->get();
    }

    public function render()
    {
        return view('livewire.posts',['posts' => $this->isLoaded ?Post::query()->latest()->get(): []]);
    }
}
